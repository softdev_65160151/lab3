package com.mycompany.lab3;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Pare
 */
public class XOUnitTest {
    
    public XOUnitTest() {
    }

   
    
    @BeforeAll
    public static void setUpClass() {
        
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWin_O_Horizontal1_output_true(){
        String[][] table = {{"O","O","O"},{"-","-","-"},{"-","-","-"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true,result);
        
    }
    @Test
    public void testCheckWin_O_Horizontal2_output_true(){
        String[][] table = {{"-","-","-"},{"O","O","O"},{"-","-","-"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true,result);  
    }
    
    @Test
    public void testCheckWin_O_Horizontal3_output_true(){
        String[][] table = {{"-","-","-"},{"-","-","-"},{"O","O","O"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true,result);  
    }
    
    @Test
    public void testCheckWin_X_Horizontal1_output_true(){
        String[][] table = {{"X","X","X"},{"-","-","-"},{"-","-","-"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true,result);  
    }
    
    @Test
    public void testCheckWin_X_Horizontal2_output_true(){
        String[][] table = {{"-","-","-"},{"X","X","X"},{"-","-","-"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true,result);  
    }
    
    @Test
    public void testCheckWin_X_Horizontal3_output_true(){
        String[][] table = {{"-","-","-"},{"-","-","-"},{"X","X","X"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true,result);  
    }
    
    @Test
    public void testCheckWin_O_Vertical1_output_true(){
        String[][] table = {{"O","-","-"},{"O","-","-"},{"O","-","-"}};
        boolean result = Lab3.checkWin(table);

        assertEquals(true,result);  
    }
    
    @Test
    public void testCheckWin_O_Vertical2_output_true(){
        String[][] table = {{"-","O","-"},{"-","O","-"},{"-","O","-"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true,result);  
    }
    
    @Test
    public void testCheckWin_O_Vertical3_output_true(){
        String[][] table = {{"-","-","O"},{"-","-","O"},{"-","-","O"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true,result);  
    }
    @Test
    public void testCheckWin_X_Vertical1_output_true(){
        String[][] table = {{"X","-","-"},{"X","-","-"},{"X","-","-"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true,result);  
    }
    
    @Test
    public void testCheckWin_X_Vertical2_output_true(){
        String[][] table = {{"-","X","-"},{"-","X","-"},{"-","X","-"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true,result);  
    }
    
    @Test
    public void testCheckWin_X_Vertical3_output_true(){
        String[][] table = {{"-","-","X"},{"-","-","X"},{"-","-","X"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true,result);  
    }
    
    @Test
    public void testCheckWin_X_DiagonalL_output_true(){
        String[][] table = {{"X","-","-"},{"-","X","-"},{"-","-","X"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true,result);  
    }
    
    @Test
    public void testCheckWin_O_DiagonalL_output_true(){
        String[][] table = {{"O","-","-"},{"-","O","-"},{"-","-","O"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true,result);  
    }
    
    @Test
    public void testCheckWin_X_DiagonalR_output_true(){
        String[][] table = {{"-","-","X"},{"-","X","-"},{"X","-","-"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true,result);  
    }
    
    @Test
    public void testCheckWin_O_DiagonalR_output_true(){
        String[][] table = {{"-","-","O"},{"-","O","-"},{"O","-","-"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true,result);  
    }
    
    @Test
    public void testCheckDraw_output_true(){
        String[][] table = {{"O","X","O"},{"X","X","O"},{"O","O","X"}};
        boolean result = Lab3.checkDraw(table);
        assertEquals(true,result);  
    }
    
    @Test
    public void testCheckDraw2_output_true(){
        String[][] table = {{"O","O","X"},{"X","X","O"},{"O","X","O"}};
        boolean result = Lab3.checkDraw(table);
        assertEquals(true,result);  
    }
    
    @Test
    public void testCheckDraw3_output_true(){
        String[][] table = {{"X","O","O"},{"O","X","X"},{"X","X","O"}};
        boolean result = Lab3.checkDraw(table);
        assertEquals(true,result);  
    }
}
